const Product = require("../models/Product");
const User = require("../models/User");



// =================== Created Product (ADMIN) ======================

module.exports.createProduct = (reqBody) => {

  const product = new Product({
    name: reqBody.name,

    description: reqBody.description,

    price: reqBody.price

  });

  return product.save().then((savedProduct) => savedProduct);
};

// ========== Retrieve all products =====================
module.exports.getAllProducts = () => {

	return Product.find({}).then(result => result)
		.catch(err => re.send(err));
}

// ========== Retrieve all active products =====================
module.exports.getAllActiveProducts = () => {

	return Product.find({ isActive : true}).then(result => result)
		.catch(err => re.send(err));
}

// ================ Get Product =========================
module.exports.getProduct = (reqParams) => {

  return Product.findById(reqParams.productId).then(result => {
    return result
  }).catch(err => re.send(err))
};


// ================= Update Product =====================
module.exports.updateProduct = (reqParams, reqBody) => {


  let updatedProduct = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price
  };

  return Product.findByIdAndUpdate(reqParams.productId, updatedProduct, {new: true}).then(updatedProduct => { //for future me -to display full data, must return updated product data

      return updatedProduct;
    })

    .catch(err => {
      return err;
    });
};

// ================= Archiving Products ====================
module.exports.archiveProduct = (reqParams) => {

  let updateActiveField = {
    isActive : false
  };

  return Product.findByIdAndUpdate(reqParams.productId, updateActiveField, {new : true})
    .then(updatedProduct => { //for future me -to display full data, must return updated product data

      return updatedProduct;
    })

    .catch(err => {
      return err;
    });
};

// =================== Activate Product ==============
module.exports.activateProduct = (reqParams) => {

  let updateActiveField = {
    isActive : true
  };

  return Product.findByIdAndUpdate(reqParams.productId, updateActiveField, {new : true})
    .then(updatedProduct => { //for future me -to display full data, must return updated product data

      return updatedProduct;
    })

    .catch(err => {
      return err;
    });
};

