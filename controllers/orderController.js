
const User = require("../models/User");
const Orders = require("../models/Order");
const Products = require("../models/Product");

// ==================== Create Order ======================

module.exports.order = async (data) => {

  let user = await User.findById(data.userId);

  if (!user) {
    return false;
  }

  user.orderedProducts.push(data.products);

  let product = await Products.findById(data.productId);

  if (!product) {
    return false;
  }

  product.userOrders.push({ userId: data.userId });

  try {
    await user.save();

    await product.save();

    return `Successfully ordered`;

  } catch (err) {

    console.error(err);

    return false;
  }
}



// ================ Check-out =============================

// ====== to order one product only ===============
// module.exports.checkOut = (userId, reqBody) => {
//   const product = reqBody.productId;
//   const quantity = reqBody.quantity;
//   const price = reqBody.price;
//   const totalAmount = quantity * price;

//   const userOrder = new Orders({
//     products: [{ productId: product, quantity, price }],
//     totalAmount,
//   });

//   return userOrder
//     .save()
//     .then((result) => {
//       return { quantity, price: totalAmount };
//     })
//     .catch((err) => err);
// };

module.exports.checkOut = (userId, products) => {
  const orderProducts = [];

  let totalAmount = 0;

  products.forEach((product) => {
    orderProducts.push({
      productId: product.productId,
      quantity: product.quantity,
      price: product.price,
    });
    totalAmount += product.quantity * product.price;
  });

  const order = new Orders ({
    userId: userId,
    products: orderProducts,
    totalAmount: totalAmount,
  });

  return order
    .save()
    .then((result) => {
      return { orderId: result._id, totalAmount: result.totalAmount };
    })
    .catch((err) => err);
};

