const bcrypt = require("bcrypt");
const User = require("../models/User");
const auth = require("../auth");
const Products = require("../models/Product")



// =============== USER REGISTER - CHECK EMAIL ===============
module.exports.checkEmailExists = (email) => {
    return User.findOne({ email: email }).then((user) => {
        return user !== null;
    }).catch((err) => {
        return err;
    });
};

// =============== USER REGISTER ==================
module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then(user => {

		if(user){
			return `User Successfully registered`
		} else {
			return false
		}
	}) .catch(err => res.send(err))
};

// ============= User Authentication ===============
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email })
    .then(result => {
      if (result == null) {
        return { error: 'User not found' };
      } else {
        const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
        if (isPasswordCorrect) {
          return { access: auth.createAccessToken(result) };
        } else {
          return { error: 'Invalid password' };
        }
      }
    })
    .catch(err => {
      console.log(err);
      err.message = 'Error in loginUser';
      return { error: err.message };
    });
};






// ================= Retrieve User Details =============


module.exports.getUser = (data) => {

  return User.findById(data.userId)

    .select("-orderedProducts -password") // exluding orderedProducts and password
    .then(result => { return result; }).catch(err => res.send(err));
};


// ==================== SET AS ADMIN =======================

module.exports.setAsAdmin = async function(data, res) {
  const user = await User.findByIdAndUpdate(
    { _id: data.userId },
    { $set: { isAdmin: true } },
    { new: true }
  );

  if (!user) {
    return res.status(404).json({ message: 'User not found' });
  }

  return res.json({ message: 'User updated successfully' });
};











