const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");
const cors = require("cors")

// =========== USER REGISTRATION =======

router.post("/register", (req, res) => {

    userController.checkEmailExists(req.body.email).then((emailExists) => {


        if (emailExists) {
            res.send("Email is already registered");

        } else {

            userController.registerUser(req.body).then((resultFromController) => {
                res.send(resultFromController);
            }).catch(err => 
                res.send(err));
            ;
        }
    }).catch(err => 
        res.send(err));
    
});


// ========== User Authentication ==========
router.post('/login', (req, res) => {

  userController.loginUser(req.body).then(resultFromController => {

      if (resultFromController.error) { //codes to show errors if output shows blank array

        res.status(401).send(resultFromController);
      } else {

        res.send(resultFromController);
      }
    }).catch(err => {

      console.log(err); //codes to show errors if output shows blank array

      res.send({ error: 'Internal Server Error' });
    });
});





// ================= Retrieve User Details =============
router.get("/:userId/profile", auth.verify, (req, res) => {

  const userData = auth.decode(req.headers.authorization);

  userController.getUser({ userId: userData.id }).then(resultFromController => res.send(resultFromController))

    .catch(err => res.send(err));

});


// ===================== SET as ADMIN =====================

router.put("/:userId/admin", auth.verify, (req, res) => {

     const userData = auth.decode(req.headers.authorization);
     
  userController.setAsAdmin({ userId: req.params.userId }, res);
});

module.exports = router;