const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");
const cors = require("cors")

// ================== Create Order ====================
router.post("/", auth.verify, (req, res) => {

  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin === false) {
    const data = {

      userId: userData.id,

      productId: req.body.productId,
    };

    orderController.order(data).then((resultFromController) => {

        console.log(resultFromController);

        res.send(resultFromController);
      })
      .catch((err) => {

        console.error(err); //codes to show errors if output shows blank array

        res.status(500).send("failed to process order");
      });
  } else {

    return res.send(false);
  }
});




// ============== Check Out =================
router.post("/:userId/checkout", auth.verify, (req, res) => {

  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin === false) {

    const userId = req.params.userId

   orderController.checkOut(userId, req.body)

      .then(resultFromController => res.send(resultFromController))

      .catch(err => res.send(err));
    } else {

      return res.send(false);
    }
});




module.exports = router;